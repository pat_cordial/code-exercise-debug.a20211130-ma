# Code Debug exercise

The intent of this exercise is primarily to get a feel for your your approach to debugging and optimizing software.
There are no real right or wrong solutions, and the intent is not for you to spend many hours working on this.  That said, the task is purposely left slightly vague to allow you to approach the problem in the manner you see fit.

## The task:
Below there will be 2 key sections.  
1. A setup guide to help you get the basic application environment running, and some sample/seed data in place. 
2. A few reports of 'platform issues'.

After getting your application environment up and running, read through the bug reports and try to evaluate and/or address the reports as you see fit.
How you approach this is largely up to you.  
However, possibly the most important thing is to **_document your process_**, as described in the next paragraph.

## Document your process
As this is not an "over-the-shoulder" coding exercise, we ask that, in addition to any code changes, you also provide a written description 
of the processes you went through and tools you used.   -Again no right or wrongs here, just trying to learn about your approach. 
The more notes the better.

Examples of things you might include in your written notes could be things like:
```
* After completing all the setup steps
* First, used curl to make X API request to confirm I understood how to use the API myself and that I got the results I expected
* Next I used Postman to try to reproduce the client’s reported issue with the API
...
or

* To further trace the issue, I went to function Y and var_dump()ed what it was receiving / installed xdebug and put in a breakpoint / wrote to the error_log()/ etcetera.
* After correcting the bug I added a doc-block to the function Y to make it easier to understand/use
* wrote test Z to prevent this bug from appearing again in the future.
...
or

* After using tool X to monitor timing of API request W, I Identified an O(n2) algorithm in function Q that was slowing things down.
* Either "I did myslef" or "we should recommend a dev" refactor function Q and function R like so, as it will make now Q is O(n) efficient.
...
or

* I used Chrome developer tools to verify I could reproduce the client’s reported response from the API
* After tracing the response to function L I determined this is expected behaviour because of Z.  
* Suggest providing the client the following information “.....”
```


----------

## Setup
**Pre-requisites:**
This setup guide as described requires docker/docker-compose to be installed on your machine.
(You’re welcome to deploy the application onto another platform if you prefer, but to keep things simple, this guide will assume the use of Docker). 
The docker-compose file also sets three containers (named web, mysqlDB, and phpmyadmin) to listen on localhost ports 80 and 3306, and 8080 respectively.
So be aware, if you have other applications or containers already running on your machine listening on those ports, there may be a conflict.


Run the following commands in a terminal from the base folder of the codebase:

####Launch the application containers:
Run:   
`docker-compose up` 

Now that the containers are running, open a second terminal window, 
to execute some commands inside the running container. 

Run:  
`docker-compose exec web bash -c "cd /var/www && bash quick_setup.sh"`

_(this just 'builds' the php app in the web container and runs some extra commands to seed the app's database with some working data)_

You should now be able to open a browser and see the application running at
http://localhost/  
-this base url will just say "Lumen (5.5.2) (Laravel Components 5.5.*)"

----------
### Quick documentation on the application/API itself:

There are basically just 3 API calls supported.  
POST `/register`  
  - Accepts a json payload containing 3 keys:  username, email, and password
  - Creates a new user account.  Email must be unique.
  - example: `docker-compose exec web curl -vX POST "http://localhost/register" -d "{  \"username\": \"jane\",  \"email\": \"janedoe@example.com\",  \"password\": \"test1234\"}" -H  "accept: application/json" -H  "Content-Type: application/json"`
  
POST `/login`     
  - Accepts a json payload with two keys: email and password
  - If email and password are correct, returns the user with an api_token for use with future /user API endpoint calls.
  - example: `docker-compose exec web curl -vX POST "http://localhost/login" -d "{  \"email\": \"janedoe@example.com\",  \"password\": \"test1234\"}" -H  "accept: application/json" -H  "Content-Type: application/json"`

GET `/user`   
  - Accepts a query string parameter api_token  which shold be set to the api_token retrieved from the /login API call.
  - Returns information about the user account that goes with that api_token.
  - example `docker-compose exec web curl -v "http://localhost/user?api_token=76dc08e947d9aecea4c269c6f2e13cd170f0f46d" -H  "accept: application/json"`

For convenience, a phpmyadmin database tool is also available at http://localhost:8080/ (unless you changed the .env file, the database in use is the `test_db` database.)


----------
At this point you should be ready to address the below platform issue reports:

## Platform issue Reports:
1. From John:  
 > My requests to the /user API are taking an extremely long time.  -Much longer than my co-worker Jane’s /user requests.  What is wrong with my account?
 > I’m pulling my user with
 >   POST: [http://localhost/login](http://localhost/login) (passing `{"email":"john-doe@example.com","password":"test5678"}`)
 >   Then use the api_token from that and put it on the /user URL like:
 >   GET: [http://localhost/user?api_token=737a4228837f01059c34899596e76c9e6e3035f9](http://localhost/user?api_token=737a4228837f01059c34899596e76c9e6e3035f9)


2. From Bob:  
 > I think I’m getting some other guy’s user info when I make the following /user API call.  What is going on?  Is someone else receiving my user information!?
 >	
 >	[http://localhost/user?api_token](http://localhost/user?api_token)


3. From Jane:
 > I’ve made the following API post repeatedly, but after the first time I’ve never been able to get it to succeed. 
 >
 >  POST: http://localhost/register  
 >      ```{ "username": "jane",  "email": "janedoe@example.com",  "password": "test1234"}```


4. Internal:
 > There is a utility script `update_pokemon_move_damage_classes.php` that updates the poke table to fill in some 
 > extra information about pokemon damage types.  A monitor for the script itself is alerting that the script has 
 > been running much more slowly than it should.  (There have also been a few reports that it is not updating all pokemon records correctly)
 > Expected throughput is 1-2 seconds per pokemon, but the script appears to occasionally be breaching 20 seconds for a single pokemon update.)
 >
 >  You can start this script with: `docker-compose exec web bash -c "php /var/www/update_pokemon_move_damage_classes.php"`
 >   
 >  The task is to identify the problem with this running script.  (note: this script does use some 3rd-party APIs so does require your computer to have internet access)

## Extra Bonuses: 
Feel free to identify document or fix any additional performance and/or security issues you spot.  
Feel free to identify or implement any tests that you feel should be added, or generally comment on anything you like.  
-Just keep in mind the 'app' is a purposely small/simple piece of code not intended to be a production system, 
and the intent is not for you to  spend many hours on this project.

____
## Documentation references:
* This app is built on the Laravel Lumen micro-framework  [Lumen website](http://lumen.laravel.com/docs).
* The .env file is purposely included providing default passwords and connection strings for easy setup.
* The quick_setup.sh file may be useful for understanding the build process. -Running it repeatedly will reset the database to the seed for this project.



